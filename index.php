<?php 
$page='home'; 
$kanal='home'; 
$subkanal=''; 
require ('assets/inc/base.php')
?>
<?php require ($_SERVER['JOB'].'assets/inc/meta.php')?>
<?php require ($_SERVER['JOB'].'assets/inc/header.php')?>

<div id="preloader-active">
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-inner position-relative">
            <div class="preloader-circle"></div>
            <div class="preloader-img pere-text">
                <img src="assets/img/logo_kodland.png" alt="">
            </div>
        </div>
    </div>
</div>


<main>

    <div class="slider-area ">
        <div class="slider-active">
            <div class="single-slider slider-height d-flex align-items-center" style="width: 80px;" data-background="assets/img/background.png">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8">
                            <div  class="hero__caption">
                                <div class="header-btn d-none f-left d-lg-block">
                                     <h1 style="color:white; margin-bottom: 10px;">POP IT!</h1>
                                    <h2 style="color:white; margin-bottom:50px;">Selamat datang di website</h2>
                                    
                                    <a href="#" class="btn head-btn1">Start</a>
                                </div>
                            </div>
                        </div>
                    </div>       

                </div>
            </div>
        </div>
    </div>
 
<div class="pt-90 pb-120"  >
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="text-center">
                    <p style="font-weight: bold; font-size: 50px; margin-bottom: 50px;">Apa itu Kodland?</p>
                    <p style="font-weight: bold; font-size: 30px;">Kodland adalah sekolah programming untuk anak-anak</p>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="pt-150 pb-150" style="background-color: #00FFFF;">
    <div class="container">
        <!-- Section Tittle -->
        <div class="row">
            <div class="col-lg-12">
                <div class="section-tittle white-text text-center">
                    <h2 style="color: black;"> Produk</h2>
                </div>
            </div>
        </div>
       
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="text-center mb-30">
                    <div class="process-ion">
                        <img style="width: 100px; margin-bottom: 30px;" src="assets/img/square.png">
                    </div>
                    <div class="process-cap">
                     <h5>1. Square</h5>
                     <p>Produk Square.</p>
                     <a href="#" class="btn head-btn1" style="background-color: orange;">Add to Cart</a>
                 </div>
             </div>
         </div>
         <div class="col-lg-4 col-md-6">
            <div class=" text-center mb-30">
                <div class="process-ion">
                     <img style="width: 100px; margin-bottom: 30px;" src="assets/img/amongus.png">
                </div>
                <div class="process-cap">
                 <h5>2. Amongus</h5>
                 <p>Produk Amongus.</p>
                 <a href="#" class="btn head-btn1" style="background-color: orange;">Add to Cart</a>
             </div>
         </div>
     </div>
     <div class="col-lg-4 col-md-6">
        <div class="text-center mb-30">
            <div class="process-ion">
                 <img style="width: 100px; margin-bottom: 30px;" src="assets/img/round.png">
            </div>
            <div class="process-cap">
             <h5>3. Round</h5>
             <p>Produk Round.</p>
             <a href="#" class="btn head-btn1" style="background-color: orange;">Add to Cart</a>
           </div>
     </div>
 </div>
</div>
</div>
</div>

<div class="testimonial-area testimonial-padding">
    <div class="container">
        
        <div class="row d-flex justify-content-center">
            <div class="col-xl-8 col-lg-8 col-md-10">
                <div class="h1-testimonial-active dot-style">
                  
                    <div class="single-testimonial text-center">
                    
                        <div class="testimonial-caption ">
                       
                            <div class="testimonial-founder  ">
                                <div class="founder-img mb-30">
                                    <img src="assets/img/testmonial/testimonial-founder.png" alt="">
                                    <span>Azka</span>
                                    <p>Tutor Web Development</p>
                                </div>
                            </div>
                            <div class="testimonial-top-cap">
                                <p>“Develop Website dengan Framework Laravel.”</p>
                            </div>
                        </div>
                    </div>
                  
                    <div class="single-testimonial text-center">
                        
                        <div class="testimonial-caption ">
                          
                            <div class="testimonial-founder  ">
                                <div class="founder-img mb-30">
                                    <img src="assets/img/testmonial/testimonial-founder.png" alt="">
                                    <span>Willdan</span>
                                    <p>"Tuotor Mobile Application"</p>
                                </div>
                            </div>
                            <div class="testimonial-top-cap">
                                <p>“Develop Android dan IOS dengan Flutter”</p>
                            </div>
                        </div>
                    </div>
                
                    <div class="single-testimonial text-center">
                       
                        <div class="testimonial-caption ">
                          
                            <div class="testimonial-founder  ">
                                <div class="founder-img mb-30">
                                    <img src="assets/img/testmonial/testimonial-founder.png" alt="">
                                    <span>Mukholladun</span>
                                    <p>Tutor Game Development</p>
                                </div>
                            </div>
                            <div class="testimonial-top-cap">
                                <p>“Develop game dengan unity.”</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="support-company-area support-padding fix" style="background-image: url('assets/img/bg-grid.png'); margin-bottom: 50px;">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-6 col-lg-6">
                <div class="right-caption">
                  
                    <div class="section-tittle section-tittle2">
                        <span>Lean canvas</span>
                        <h2>Business development</h2>
                    </div>
                    <div class="support-caption">
                        <p class="pera-top">Membuat website dengan merancang proses bisnis dengan sebaik mungkin sehingga dapat menghasilkan revenue.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
      
                <div class="support-location-img">
                    <img src="assets/img/photo.jpg" alt="">
                    
                </div>
            </div>
        </div>
    </div>
</div>

</main>

<?php require ($_SERVER['JOB'].'assets/inc/footer.php')?>